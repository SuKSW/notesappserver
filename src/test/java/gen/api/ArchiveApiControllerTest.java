package gen.api;

import gen.model.Note;
import gen.model.NotesList;
import impl.NotesService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static net.bytebuddy.matcher.ElementMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringJUnit4ClassRunner.class)
public class ArchiveApiControllerTest {

    private MockMvc mockMvc;

    @Mock
    private NotesService notesServiceMock;

    @Spy
    @InjectMocks
    private ArchiveApiController archiveApiController;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders
                .standaloneSetup(archiveApiController)
                .build();
    }

    @Test
    public void archiveGet_success() throws Exception {
        List<Note> notes = Arrays.asList(
                new Note().id("1234").title("test title"),
                new Note().id("1235").title("test title2"));
        NotesList notesList = new NotesList().list(notes).count(2);
        when(archiveApiController.getAcceptHeader()).thenReturn(Optional.of("application/json"));
        when(notesServiceMock.listArchivedNotes(10, 0)).thenReturn(notesList);

        mockMvc.perform(get("/archive").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.count").value(2))
                .andExpect(jsonPath("$.list").value(hasSize(2)))
                .andExpect(jsonPath("$.list[0].id").value("1234"))
                .andExpect(jsonPath("$.list[0].title").value("test title"))
                .andExpect(jsonPath("$.list[1].id").value("1235"))
                .andExpect( jsonPath("$.list[1].title").value("test title2"));
        verify(notesServiceMock, times(1)).listArchivedNotes(10, 0);
        verifyNoMoreInteractions(notesServiceMock);
    }
}