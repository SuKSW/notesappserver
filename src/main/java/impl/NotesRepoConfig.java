package impl;

public interface NotesRepoConfig {
    String getCollectionName();
    void setCollectionName(String collectionName);
}
