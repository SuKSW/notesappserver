package impl;

import org.springframework.stereotype.Component;

@Component("notesRepoConfig")
public class NotesRepoConfigImpl implements NotesRepoConfig {

    private static String collectionName = "saved";
    @Override
    public String getCollectionName() {
        return collectionName;
    }
    @Override
    public void setCollectionName(String collectionName) {
        this.collectionName = collectionName;
    }
}
