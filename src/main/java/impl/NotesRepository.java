package impl;

import gen.model.Note;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;


public interface NotesRepository extends MongoRepository<Note, String> {

    @Query(value = "{}", fields="{'note' : 0}")
    Page<Note> findTitles(Pageable pageable);
}
