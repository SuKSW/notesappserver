package impl;

import gen.api.ApiException;
import gen.model.ArchiveAction;
import gen.model.Note;
import gen.model.NotesList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Component;

import java.time.OffsetDateTime;
import java.time.temporal.ChronoUnit;

@Component
public class NotesService {
    @Autowired
    private NotesRepository notesRepo;

    @Autowired
    private ArchiveRepository archiveRepo;

    @Autowired
    private NotesRepoConfig notesRepoConfig;

    private static final String ARCHIVE = "archive";
    private static final String UNARCHIVE = "unarchive";

    private static final String SAVED = "saved";
    private static final String ARCHD = "archd";


    /**
     *  Connected to NotesRepository
     * */

    public NotesList listNotes(Integer limit, Integer offset){
        NotesList notesList = new NotesList();
        notesRepoConfig.setCollectionName(SAVED);
        Page<Note> page = notesRepo.findTitles(new PageRequest(offset, limit));
        notesList.setCount(page.getNumberOfElements());
        notesList.setList(page.getContent());

        if(page.hasNext()){
            notesList.setNext("/notes?limit="+ limit +"&offset="+ (offset+limit));
        }
        if(page.hasPrevious()){
            if((offset-limit)>=0) {
                notesList.setPrevious("/notes?limit=" + limit + "&offset=" + (offset-limit));
            } else {
                notesList.setPrevious("/notes?limit=" + limit + "&offset=0");
            }
        }
        return notesList;
    }

    public Note createNote(Note note) {
        Note inNote = new Note()
                .title(note.getTitle())
                .note(note.getNote())
                .timeCreated(OffsetDateTime.now().truncatedTo(ChronoUnit.SECONDS).toString());
        notesRepoConfig.setCollectionName(SAVED);
        return notesRepo.insert(inNote);
    }

    public Note viewNote(String noteId) {
        notesRepoConfig.setCollectionName(SAVED);
        return notesRepo.findOne(noteId);
    }

    public Note updateNote(String noteId, Note note){
        Note inNote = new Note()
                .id(noteId)
                .title(note.getTitle())
                .note(note.getNote())
                .lastUpdated(OffsetDateTime.now().truncatedTo(ChronoUnit.SECONDS).toString());
        notesRepoConfig.setCollectionName(SAVED);
        return notesRepo.save(inNote);
    }

    public void deleteNote(String noteId){
        notesRepoConfig.setCollectionName(SAVED);
        notesRepo.delete(noteId);
    }

    /**
     *  Connected to ArchiveRepository
     * */

    public NotesList listArchivedNotes(Integer limit, Integer offset){
        NotesList notesList = new NotesList();
        notesRepoConfig.setCollectionName(ARCHD);
        Page<Note> page = archiveRepo.findTitles(new PageRequest(offset, limit));
        notesList.setCount(page.getNumberOfElements());
        notesList.setList(page.getContent());

        if(page.hasNext()){
            notesList.setNext("/archive?limit="+ limit +"&offset="+ (offset+limit));
        }
        if(page.hasPrevious()){
            if((offset-limit)>=0) {
                notesList.setPrevious("/archive?limit=" + limit + "&offset=" + (offset-limit));
            } else {
                notesList.setPrevious("/archive?limit=" + limit + "&offset=0");
            }
        }
        return notesList;
    }

    /**
     *  Connected to both NotesRepository and ArchiveRepository
     * */
    public ArchiveAction executeArchiveAction(String noteId, String action) throws ApiException {
        switch (action){
            case ARCHIVE:
                return archive(noteId);
            case UNARCHIVE:
                return unarchive(noteId);
            default:
                throw new ApiException(400, "Invalid archive action provided");
        }
    }

    private ArchiveAction archive(String noteId) {
        notesRepoConfig.setCollectionName(SAVED);
        Note note = notesRepo.findOne(noteId);
        notesRepo.delete(noteId);

        notesRepoConfig.setCollectionName(ARCHD);
        archiveRepo.insert(note);

        return new ArchiveAction()
                .noteId(noteId)
                .noteStatus(ArchiveAction.NoteStatusEnum.ARCHIVED);
    }

    private ArchiveAction unarchive(String noteId) {
        notesRepoConfig.setCollectionName(ARCHD);
        Note note = archiveRepo.findOne(noteId);
        archiveRepo.delete(noteId);

        notesRepoConfig.setCollectionName(SAVED);
        notesRepo.insert(note);

        return new ArchiveAction()
                .noteId(noteId)
                .noteStatus(ArchiveAction.NoteStatusEnum.UNARCHIVED);
    }
}
