package gen.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import gen.model.Note;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * NotesList
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-24T18:06:40.183+05:30")

public class NotesList   {
  @JsonProperty("count")
  private Integer count = null;

  @JsonProperty("next")
  private String next = null;

  @JsonProperty("previous")
  private String previous = null;

  @JsonProperty("list")
  @Valid
  private List<Note> list = null;

  public NotesList count(Integer count) {
    this.count = count;
    return this;
  }

  /**
   * Number of Notes returned. 
   * @return count
  **/
  @ApiModelProperty(example = "1", value = "Number of Notes returned. ")


  public Integer getCount() {
    return count;
  }

  public void setCount(Integer count) {
    this.count = count;
  }

  public NotesList next(String next) {
    this.next = next;
    return this;
  }

  /**
   * Link to the next subset of resources qualified. Empty if no more resources are to be returned. 
   * @return next
  **/
  @ApiModelProperty(example = "/notes?limit=1&offset=2", value = "Link to the next subset of resources qualified. Empty if no more resources are to be returned. ")


  public String getNext() {
    return next;
  }

  public void setNext(String next) {
    this.next = next;
  }

  public NotesList previous(String previous) {
    this.previous = previous;
    return this;
  }

  /**
   * Link to the previous subset of resources qualified. Empty if current subset is the first subset returned. 
   * @return previous
  **/
  @ApiModelProperty(example = "/notes?limit=1&offset=0", value = "Link to the previous subset of resources qualified. Empty if current subset is the first subset returned. ")


  public String getPrevious() {
    return previous;
  }

  public void setPrevious(String previous) {
    this.previous = previous;
  }

  public NotesList list(List<Note> list) {
    this.list = list;
    return this;
  }

  public NotesList addListItem(Note listItem) {
    if (this.list == null) {
      this.list = new ArrayList<>();
    }
    this.list.add(listItem);
    return this;
  }

  /**
   * Get list
   * @return list
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<Note> getList() {
    return list;
  }

  public void setList(List<Note> list) {
    this.list = list;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    NotesList notesList = (NotesList) o;
    return Objects.equals(this.count, notesList.count) &&
        Objects.equals(this.next, notesList.next) &&
        Objects.equals(this.previous, notesList.previous) &&
        Objects.equals(this.list, notesList.list);
  }

  @Override
  public int hashCode() {
    return Objects.hash(count, next, previous, list);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class NotesList {\n");
    
    sb.append("    count: ").append(toIndentedString(count)).append("\n");
    sb.append("    next: ").append(toIndentedString(next)).append("\n");
    sb.append("    previous: ").append(toIndentedString(previous)).append("\n");
    sb.append("    list: ").append(toIndentedString(list)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

