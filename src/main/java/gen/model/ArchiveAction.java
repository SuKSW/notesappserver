package gen.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * ArchiveAction
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-24T18:06:40.183+05:30")

public class ArchiveAction   {
  @JsonProperty("noteId")
  private String noteId = null;

  /**
   * Whether the note was archived or unarchived.
   */
  public enum NoteStatusEnum {
    ARCHIVED("archived"),
    
    UNARCHIVED("unarchived");

    private String value;

    NoteStatusEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static NoteStatusEnum fromValue(String text) {
      for (NoteStatusEnum b : NoteStatusEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }
  }

  @JsonProperty("noteStatus")
  private NoteStatusEnum noteStatus = null;

  public ArchiveAction noteId(String noteId) {
    this.noteId = noteId;
    return this;
  }

  /**
   * Get noteId
   * @return noteId
  **/
  @ApiModelProperty(example = "vrf67767577", required = true, value = "")
  @NotNull


  public String getNoteId() {
    return noteId;
  }

  public void setNoteId(String noteId) {
    this.noteId = noteId;
  }

  public ArchiveAction noteStatus(NoteStatusEnum noteStatus) {
    this.noteStatus = noteStatus;
    return this;
  }

  /**
   * Whether the note was archived or unarchived.
   * @return noteStatus
  **/
  @ApiModelProperty(example = "archived", required = true, value = "Whether the note was archived or unarchived.")
  @NotNull


  public NoteStatusEnum getNoteStatus() {
    return noteStatus;
  }

  public void setNoteStatus(NoteStatusEnum noteStatus) {
    this.noteStatus = noteStatus;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ArchiveAction archiveAction = (ArchiveAction) o;
    return Objects.equals(this.noteId, archiveAction.noteId) &&
        Objects.equals(this.noteStatus, archiveAction.noteStatus);
  }

  @Override
  public int hashCode() {
    return Objects.hash(noteId, noteStatus);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ArchiveAction {\n");
    
    sb.append("    noteId: ").append(toIndentedString(noteId)).append("\n");
    sb.append("    noteStatus: ").append(toIndentedString(noteStatus)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

