package gen.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import gen.model.ArchiveAction;
import gen.model.NotesList;
import impl.NotesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-24T09:44:41.806+05:30")

@Controller
public class ArchiveApiController implements ArchiveApi {

    @Autowired
    private NotesService notesService;

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    @org.springframework.beans.factory.annotation.Autowired
    public ArchiveApiController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }

    @Override
    public Optional<ObjectMapper> getObjectMapper() {
        return Optional.ofNullable(objectMapper);
    }

    @Override
    public Optional<HttpServletRequest> getRequest() {
        return Optional.ofNullable(request);
    }

    @Override
    public ResponseEntity<NotesList> archiveGet(@RequestParam(value = "limit", required = false, defaultValue="10") Integer limit, @RequestParam(value = "offset", required = false, defaultValue="0") Integer offset) {
        if(getAcceptHeader().isPresent()) {
            if (getAcceptHeader().get().contains("application/json")) {
                return new ResponseEntity<NotesList>(notesService.listArchivedNotes(limit, offset), HttpStatus.OK);
            }
        }
        log.error("err");
        return new ResponseEntity<>( HttpStatus.UNSUPPORTED_MEDIA_TYPE);
    }

    @Override
    public ResponseEntity<ArchiveAction> archivePost(String noteId, String action) {
        if(getAcceptHeader().isPresent()) {
            if (getAcceptHeader().get().contains("application/json")) {
                try {
                    return new ResponseEntity<ArchiveAction>(notesService.executeArchiveAction(noteId, action), HttpStatus.OK);
                } catch (ApiException e) {
                    log.error("Error while executing archive action", e);
                    return new ResponseEntity<>(HttpStatus.UNSUPPORTED_MEDIA_TYPE);
                }
            }
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
}
