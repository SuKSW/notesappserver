package gen.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import gen.model.Note;
import gen.model.NotesList;
import impl.NotesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-24T09:44:41.806+05:30")

@Controller
public class NotesApiController implements NotesApi {

    @Autowired
    private NotesService notesService;

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    @org.springframework.beans.factory.annotation.Autowired
    public NotesApiController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }

    @Override
    public Optional<ObjectMapper> getObjectMapper() {
        return Optional.ofNullable(objectMapper);
    }

    @Override
    public Optional<HttpServletRequest> getRequest() {
        return Optional.ofNullable(request);
    }

    @Override
    public ResponseEntity<NotesList> notesGet(@RequestParam(value = "limit", required = false, defaultValue="10") Integer limit, @RequestParam(value = "offset", required = false, defaultValue="0") Integer offset) {
        if(getAcceptHeader().isPresent()) {
            if (getAcceptHeader().get().contains("application/json")) {
                return new ResponseEntity<NotesList>(notesService.listNotes(limit, offset), HttpStatus.OK);
            }
        }
        return new ResponseEntity<>(HttpStatus.UNSUPPORTED_MEDIA_TYPE);
    }

    @Override
    public ResponseEntity<Void> notesNoteIdDelete(String noteId) {
        if(getAcceptHeader().isPresent()) {
            notesService.deleteNote(noteId);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @Override
    public ResponseEntity<Note> notesNoteIdGet(String noteId) {
        if(getAcceptHeader().isPresent()) {
            if (getAcceptHeader().get().contains("application/json")) {
                return new ResponseEntity<Note>(notesService.viewNote(noteId), HttpStatus.OK);
            }
        }
        return new ResponseEntity<>(HttpStatus.UNSUPPORTED_MEDIA_TYPE);
    }

    @Override
    public ResponseEntity<Note> notesNoteIdPut(String noteId, @RequestBody Note note) {
        if(getAcceptHeader().isPresent()) {
            if (getAcceptHeader().get().contains("application/json")) {
                return new ResponseEntity<Note>(notesService.updateNote(noteId, note), HttpStatus.OK);
            }
        }
        return new ResponseEntity<>(HttpStatus.UNSUPPORTED_MEDIA_TYPE);
    }

    @Override
    public ResponseEntity<Note> notesPost(@RequestBody Note note) {
        if(getAcceptHeader().isPresent()) {
            if (getAcceptHeader().get().contains("application/json")) {
                return new ResponseEntity<Note>(notesService.createNote(note), HttpStatus.CREATED);
            }
        }
        return new ResponseEntity<>(HttpStatus.UNSUPPORTED_MEDIA_TYPE);
    }
}
