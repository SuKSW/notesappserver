https://github.com/swagger-api/swagger-codegen/tree/master
wget http://central.maven.org/maven2/io/swagger/swagger-codegen-cli/2.4.10/swagger-codegen-cli-2.4.10.jar

java -jar ./resources/swagger-codegen-cli-2.4.10.jar config-help -l spring
java -jar ./resources/swagger-codegen-cli-2.4.10.jar generate -i ./src/main/resources/api.yaml -l spring -o ./ -c ./resources/swagger_codegen_config.json

mvn spring-boot:run

sudo service mongod start
sudo service mongod status
sudo service mongod stop
sudo service mongod restart