# Personal Notes Application Server
This is a REST API backend application that can be used to manage personal notes in a multi-user 
environment. The API has the following key features:

1. Save a new note
2. Update a previously saved note
3. Delete a saved note
4. Archive a note
5. Unarchive a previously archived note
6. List saved notes that aren't archived (returns the titles of the notes)
7. List notes that are archived (returns the titles of the notes)
8. View a given note (returns the complete note)

OAuth2 is set as the security in order to support a multi-user environment.

## Prerequisites
1. Java 8+
2. Apache maven 3.3.3 or greater
3. MongoDB - https://docs.mongodb.com/manual/tutorial/install-mongodb-on-ubuntu/

## To run the server
1. Start the Mongo DB service   
```sudo service mongod start```
2. Check is the Mongo DB service is active
```sudo service mongod status```
3. Clone this repo
```git clone https://gitlab.com/SuKSW/notesappserver.git```
4. Run the server
```
cd notesappserver
mvn spring-boot:run
```
    
### API docs
To view docs navigate to http://localhost:8080/notesApp

### Maintenance
1. Edit ```./src/main/resources/api.yaml```
2. Regenerate the code in ./src/main/java/gen using the following command
    ```
    cd notesappserver
    java -jar ./resources/swagger-codegen-cli-2.4.10.jar generate -i ./src/main/resources/api.yaml -l spring -o ./ -c ./resources/swagger_codegen_config.json
    ```
    This excludes regeneration of the controllers due to the interface only option set in ```./resources/swagger_codegen_config.json```.
Also the files given in ```.swagger-codegen-ignore```
3. Update the controller files according to the changed added to the auto generated apis

### Further improvements that can be added

1. Add error responses with details
2. Add tests to have a coverage of more than 80%
3. Implementation of the authentication token generation process and its validation
4. Components to check the safety on the requests inputs


## Overview  
This server was generated by the [swagger-codegen](https://github.com/swagger-api/swagger-codegen) project.  
By using the [OpenAPI-Spec](https://github.com/swagger-api/swagger-core), you can easily generate a server stub.  
This is an example of building a swagger-enabled server in Java using the SpringBoot framework.  

The underlying library integrating swagger to SpringBoot is [springfox](https://github.com/springfox/springfox)  

Change default port value in application.properties

